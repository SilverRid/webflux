package com.webflux.su.webflux.controller;

import com.webflux.su.webflux.model.SendMessageRequest;
import com.webflux.su.webflux.service.MessageService;
import com.webflux.su.webflux.service.NotifyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Slf4j
@RestController
@RequestMapping("/sse")
@RequiredArgsConstructor
public class NotifyController {

	private final NotifyService notifyService;
	private final MessageService messageService;

	@GetMapping(path = "/subscribe/{subscriber}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<ServerSentEvent> openSubscribe(@PathVariable String subscriber, ServerHttpRequest request) {
		return notifyService.openStream(subscriber, request);
	}

	@PutMapping(path = "/broadcast")
	public void sendMessageForAll(@RequestBody SendMessageRequest request) {
		log.info(String.format("%s\t%s", LocalDateTime.now(), request.getMessage()));
		messageService.sendMessageForAll(request);
	}

	@PutMapping(path = "/notify/{subscriber}")
	public void sendMessageByName(@PathVariable String subscriber,
								  @RequestBody SendMessageRequest request) {
		messageService.sendMessageByData(subscriber, request);
	}


}

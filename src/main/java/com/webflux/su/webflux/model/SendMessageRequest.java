package com.webflux.su.webflux.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Getter
@Setter
@NoArgsConstructor
public class SendMessageRequest {
	private String message;
}


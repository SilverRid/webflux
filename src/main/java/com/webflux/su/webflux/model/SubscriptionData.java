package com.webflux.su.webflux.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.codec.ServerSentEvent;
import reactor.core.publisher.FluxSink;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Getter
@Setter
@AllArgsConstructor
public class SubscriptionData {
	private String address;
	private String userName;
	private FluxSink<ServerSentEvent> fluxSink;
}


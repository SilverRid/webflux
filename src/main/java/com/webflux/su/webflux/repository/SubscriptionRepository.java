package com.webflux.su.webflux.repository;

import com.webflux.su.webflux.model.SubscriptionData;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Repository
public class SubscriptionRepository {

	private SubscriptionRepository() {
	}

	private static Map<UUID, SubscriptionData> subscriptions;

	public static synchronized Map<UUID, SubscriptionData> getSubscriptions() {
		if (subscriptions == null) {
			subscriptions = new ConcurrentHashMap<>();
		}

		return subscriptions;
	}
}

package com.webflux.su.webflux.service;

import com.webflux.su.webflux.model.SendMessageRequest;
import com.webflux.su.webflux.model.SubscriptionData;
import com.webflux.su.webflux.repository.SubscriptionRepository;
import lombok.Getter;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Service
@Getter
public class MessageService {
	private final Map<UUID, SubscriptionData> subscriptions = SubscriptionRepository.getSubscriptions();

	public void sendMessageForAll(SendMessageRequest request){
		ServerSentEvent<String> event = ServerSentEvent
				.builder(request.getMessage())
				.build();
		subscriptions.forEach((uuid, subscriptionData) ->
				subscriptionData.getFluxSink().next(event)
		);
	}

	public void sendMessageByData(String userName, SendMessageRequest request){
		ServerSentEvent<String> event = ServerSentEvent
				.builder(request.getMessage())
				.build();

		subscriptions.forEach((uuid, subscriptionData) -> {
			if (userName.equals(subscriptionData.getUserName())) {
				subscriptionData.getFluxSink().next(event);
			}
		});
	}

}

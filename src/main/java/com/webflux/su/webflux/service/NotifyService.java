package com.webflux.su.webflux.service;

import com.webflux.su.webflux.model.SubscriptionData;
import com.webflux.su.webflux.repository.SubscriptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Sergei Iurochkin
 * @email yurochkins@fizpribor.ru
 */
@Service
@Slf4j
public class NotifyService {
	private Map<UUID, SubscriptionData> subscriptions = SubscriptionRepository.getSubscriptions();

	public Flux<ServerSentEvent> openStream(String subscriber, ServerHttpRequest request) {
		return Flux.create(fluxSink -> {
					log.info("create subscription for " + Objects.requireNonNull(request.getRemoteAddress()).getAddress().getHostName() + " " + subscriber);
					UUID uuid = UUID.randomUUID();
					fluxSink.onCancel(() -> {
						subscriptions.remove(uuid);
						log.info("subscription " + subscriber + " was closed");
					});
					SubscriptionData subscriptionData = new SubscriptionData(request.getRemoteAddress().getAddress().getHostName(), subscriber, fluxSink);
					subscriptions.put(uuid, subscriptionData);
				}
			);
	}
}
